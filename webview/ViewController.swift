//
//  ViewController.swift
//  webview
//
//  Created by Lingyu Wang on 26.05.21.
//

import UIKit
import WebKit

class ViewController: UIViewController {
    var webView: WKWebView!

    @IBOutlet weak var containerView: UIView!
    
    override func loadView() {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.allowsInlineMediaPlayback = true

        webView = WKWebView(frame: .zero, configuration: webConfiguration)

        view = webView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: "https://sample.shishiapp.com")!
        webView.load(URLRequest(url: url))
    }

}


